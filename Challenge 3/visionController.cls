public class VisionController {

    public boolean showPredictions { get; set; }

public ContentVersion cont {get;set;}
public string docid1;


    public VisionController() {
         cont = new ContentVersion();
        cont.VersionData = null;
        showPredictions = false;
       
    }    
    public String getAccessToken() {
       
        ContentVersion base64Content = [SELECT Title, VersionData FROM ContentVersion where Title='einstein_platform' OR  Title='predictive_services' ORDER BY Title LIMIT 1];
        String keyContents = EncodingUtil.base64Encode(base64Content.VersionData);
        keyContents = keyContents.replace('-----BEGIN RSA PRIVATE KEY-----', '');
        keyContents = keyContents.replace('-----END RSA PRIVATE KEY-----', '');
        keyContents = keyContents.replace('\n', '');

        // Get a new token
        JWT jwt = new JWT('RS256');
        jwt.cert = 'JWTCert'; 
        //jwt.pkcs8 = keyContents; // Comment this if you are using jwt.cert
        jwt.iss = 'developer.force.com';
        jwt.sub = 'mourya.anumula@se.com';
        jwt.aud = 'https://api.einstein.ai/v2/oauth2/token';
        jwt.exp = '3600';
        String access_token = JWTBearerFlow.getAccessToken('https://api.einstein.ai/v2/oauth2/token', jwt);
        return access_token;    
    }

    public List<Vision.Prediction> getCallVisionUrl() {
        // Get a new token
       // String access_token = getAccessToken();
    
        // Make a prediction using URL to a file
        return Vision.predictUrl('https://cdn.mos.cms.futurecdn.net/ntFmJUZ8tw3ULD3tkBaAtf-320-80.jpg','INCFANSFIFDDINSYJVFFSWBUKFCEMRCCLI2VKUSQGJKFIM2JKJCDGQKII5IDOM2UJ5MTGRCBKI3VSS2QKFIEQNCSKBIEUTSKGI3TIRKFJFLUCSKLLA2ESSCVIRHVQVKZGJMVMVZUK5GDMSSOKI2ESQK2GZLEGUBWGRDES7COIE','2NCXXHXR5OJR3SOWAYYP7MCUV4');
    }
    
    
    @AuraEnabled
    
    public static List<String> testforlwc(id docid){
        string accessTkn = getAccessToken();
        List<Vision.Prediction> lst = new List<Vision.Prediction>();
        ContentVersion content = [SELECT VersionData FROM ContentVersion WHERE ContentDocumentId =: docid AND IsLatest = true limit 1];
        lst =  Vision.predictBlob(content.VersionData,accessToken, 'Z6VZN4UAYQFAVMCASMAH75YWWY');
        List<string> str = new List<string>();
        List<productDetail__c> pr = new List<productDetail__c>();
        for(Integer i = 0;i<lst.size();i++){
        if(lst[i].probability > 0.5){
        pr = [select name,Description__c from productDetail__c where name =: lst[i].label]; 
        str.add('Product Name: '+lst[i].label);
        str.add('Matching Probability: '+lst[i].probability);
        str.add('Product Description: '+pr[i].Description__c);


        
        }
        }
        return str;
    }
    public pagereference test(){
     showPredictions = true;
     return null;
    }
    
    public List<string> getPredictfromVFP(){
     List<Vision.Prediction> lst = new List<Vision.Prediction>();
     // string accessTkn = getAccessToken();
      //system.debug('accesstoken'+accessTkn );
        //ContentVersion content = [SELECT VersionData FROM ContentVersion WHERE ContentDocumentId =: docid AND IsLatest = true limit 1];
        //blob b = Encodingutil.base64Encode(cont.VersionData);
        showPredictions = true;
        lst =  Vision.predictBlob(cont.VersionData,'IQ3EQSCMLI3EKQSTKJEFCTCJI5AUYTSSGZCEWQKQLBLEQNCGGNMFUM2RLFJFSS2XIRMDEUSTLFIE6SCLKQ3ECUJXJVFE4VKEKNMEURRTINKVSWCWI5MFGSSVKFBDMV2IINFFSS2RGJEFIQSIKE2E2TSWKZBEIUSEIZCUC7COIE', 'Z6VZN4UAYQFAVMCASMAH75YWWY');
        List<string> str = new List<string>();
                List<productDetail__c> pr = new List<productDetail__c>();
        for(Integer i = 0;i<lst.size();i++){
        if(lst[i].probability > 0.5){
         pr = [select name,Description__c from productDetail__c where name =: lst[i].label]; 
        str.add('Product Name: '+lst[i].label);
        str.add('Matching Probability: '+lst[i].probability);
       if(pr[i]!= null) str.add('Product Description: '+pr[i].Description__c);    }    }
        return str;
    }
    

    public  List<Vision.Prediction> getCallVisionContent() {
        // Get a new token
        //  String access_token = getAccessToken();
        // Make a prediction for an image stored in Salesforce
        // by passing the file as blob which is then converted to base64 string
        //ContentVersion content = [SELECT Title,VersionData FROM ContentVersion where Id = '06841000000LkfCAAS' LIMIT 1];
       
        return Vision.predictBlob(cont.VersionData,'IRNEUMSEJY3UMQSUI5EUMVCPKVGEQV2LIFEEEUCIGREEWMRWK5KE4VCXKZCDEU2KJBLFKRSHJZJUCNKUKVDEOSSMJNEFIV2TIFBDEUBVKA3DOQJUINGFQN2CLFIU6NKQGZGUSRBSIJHDIUKNJUZUYRCUINME6USUK5HUC7COIE', 'Z6VZN4UAYQFAVMCASMAH75YWWY');
    }
}